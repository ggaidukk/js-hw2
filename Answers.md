1. Number, typeof ==="number"
   String, typeof ==="string"
   BigInt, typeof ==="bigint"
   Boolean, typeof ==="boolean"
   Undefined, typeof ==="undefined"
   Null, typeof ==="object"
   Symbol, typeof ==="symbol"
   Object,typeof ==="object"
2. Оператор суворої рівності "===" перевіряє рівність без приведення типів, в той час коли оператор "==" - порівняння з приведенням до одного типу
3. Оператор - це внутрішня функція JavaScript. Під час використання оператора ми просто запускаємо ту чи іншу вбудовану функцію, яка виконує якісь певні дії та повертає результат.
